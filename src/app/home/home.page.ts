import { Component } from '@angular/core';
import { HttpClient ,HttpHeaders } from '@angular/common/http';
import { AlertController  } from '@ionic/angular';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  items: any[] = [];
  restServiceUrl = 'http://localhost:7001/api-users/users/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };
  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Alert',
      subHeader: 'Subtitle',
      message: 'This is an alert message.',
      buttons: ['OK']
    });

    await alert.present();
  }

  constructor(private http: HttpClient,public alertController: AlertController ) {
    this.consumeRest('get');
  }

  async update($event,itemId){
    console.log(itemId);
    let itemsTemp = this.items;
    const alert = await this.alertController.create({
      header: 'Edit',
      inputs: [
        {
          name: 'fristName',
          type: 'text',
          value: this.items[itemId].fristName,
          placeholder: 'Insert Frist Name'
        },
        {
          name: 'lastName',
          type: 'text',
          value: this.items[itemId].lastName,
          placeholder: 'Insert Last Name'
        },
        {
          name: 'gender',
          value: this.items[itemId].gender,
          type: 'text',
          placeholder: 'Insert Gender'
        },
        // input date with min & max
        {
          name: 'city',
          value: this.items[itemId].city,
          type: 'text',
          placeholder: 'Insert City'
        },
        // input date without min nor max
        {
          name: 'country',
          value: this.items[itemId].country,
          type: 'text',
          placeholder: 'Insert Country'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Save',
          handler: (data) => {
            itemsTemp[itemId].fristName = data.fristName;
            itemsTemp[itemId].lastName = data.lastName;
            itemsTemp[itemId].gender = data.gender;
            itemsTemp[itemId].city = data.city;
            itemsTemp[itemId].country = data.country;
            data.id = itemsTemp[itemId].id;
            this.consumeRest('update',data);
            
          }
        }
      ]
    });
    this.items =itemsTemp;
    await alert.present();
  }

  async insert($event,itemId){
    console.log(itemId);
    let itemsTemp = this.items;
    const alert = await this.alertController.create({
      header: 'Insert',
      inputs: [
        {
          name: 'fristName',
          type: 'text',
          value: "",
          placeholder: 'Insert Frist Name'
        },
        {
          name: 'lastName',
          type: 'text',
          value: "",
          placeholder: 'Insert Last Name'
        },
        {
          name: 'gender',
          value: "",
          type: 'text',
          placeholder: 'Insert Gender'
        },
        // input date with min & max
        {
          name: 'city',
          value: "",
          type: 'text',
          placeholder: 'Insert City'
        },
        // input date without min nor max
        {
          name: 'country',
          value: "",
          type: 'text',
          placeholder: 'Insert Country'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          }
        }, {
          text: 'Insert',
          handler: (data) => {
            this.consumeRest('insert',data);
            itemsTemp = [];
            this.consumeRest('get',data);
            
          }
        }
      ]
    });
    this.items =itemsTemp;
    await alert.present();
  }
  async delete(even,id){
    this.items.pop();
    this.consumeRest('delete',{id: id});
  }


  consumeRest(option,data?){
    let itemsTemp = this.items;
    switch(option){
      case 'get':
        this.http.get(this.restServiceUrl)
        .subscribe(
          (data) => { // Success
    
            for(var i =0;i < data.users.length;i++){
              itemsTemp.push({
                city: data.users[i].city,
                country: data.users[i].country,
                fristName: data.users[i].fristName,
                gender: data.users[i].gender,
                id: data.users[i].id,
                lastName: data.users[i].lastName,
                index: i
              });
            }
          },
          (error) =>{
            console.error(error);
          }
        );
      break;
      case 'update':
        this.http.post(this.restServiceUrl+"update/"+data.id,data,this.httpOptions)
        .subscribe(
          (response) => { // Success
            if(response.rc == '00'){
              alert("updated");
            }
            
            
          },
          (error) =>{
            console.error(error);
          }
        );
      break;
      case 'insert':
        this.http.post(this.restServiceUrl,data,this.httpOptions)
          .subscribe(
            (response) => { // Success
              if(response.rc == '00'){
                alert("Saved");
              }
            },
            (error) =>{
              console.error(error);
            }
          );
      break;
      case 'delete':
        this.http.get(this.restServiceUrl+"delete/"+itemsTemp[data.id].id)
        .subscribe(
          (response) => { // Success
            if(response.rc == '00'){
              itemsTemp = [];
              this.consumeRest('get');
            }
          },
          (error) =>{
            console.error(error);
          }
        );
      break;
    }
    this.items =itemsTemp;
  }

}
